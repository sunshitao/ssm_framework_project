package com.example.ssmproject.controller;

import com.example.ssmproject.model.TUser;
import com.example.ssmproject.mapper.TUserMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * (TUser)业务控制类
 * @author makejava
 * @since 2019-12-26 23:13:27
 */
@RestController
@EnableAutoConfiguration
@RequestMapping("/TUserController")
public class TUserController {

    @Autowired
    private TUserMapper TUserMapper;

    @RequestMapping("/selectById")
    public TUser selectById(HttpServletRequest request, HttpServletResponse response,
                            String userid) throws IOException {
        request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");

		return TUserMapper.selectById(userid);
    }

    @RequestMapping("/selectAllByPage")
    public String selectAllByPage(HttpServletRequest request, HttpServletResponse response,
        String userid) throws IOException {
        request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		
		return "";
    }
    
    
    @RequestMapping("/selectAll")
    public String selectAll(HttpServletRequest request, HttpServletResponse response,
        String userid) throws IOException {
        request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		
		return "";
    }

    
    @RequestMapping("/insert")
    public String insert(HttpServletRequest request, HttpServletResponse response,
        String userid) throws IOException {
        request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		
		return "";
    }
    
    @RequestMapping("/deleteById")
    public String deleteById(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		
		return "";
    }
}