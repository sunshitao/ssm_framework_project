package com.example.ssmproject.model;

import java.util.Date;
import java.io.Serializable;

/**
 * (TUser)实体类
 *
 * @author makejava
 * @since 2019-12-26 23:13:28
 */
public class TUser implements Serializable {
    private static final long serialVersionUID = -24839668642976972L;
    
    private String userid;
    
    private String username;
    
    private String realname;
    
    private Integer roleid;
    
    private String pwd;
    
    private String phone;
    
    private Integer status;
    
    private Integer photoid;
    
    private Date lastlogindate;
    
    private Date createtime;
    
    private String loginstatus;
    
    private String note;

    public TUser(){
        super();
    }
    
    public TUser(String userid, String username, String realname, Integer roleid, String pwd, String phone, Integer status, Integer photoid, Date lastlogindate, Date createtime, String loginstatus, String note){
        this.userid=userid;
        this.username=username;
        this.realname=realname;
        this.roleid=roleid;
        this.pwd=pwd;
        this.phone=phone;
        this.status=status;
        this.photoid=photoid;
        this.lastlogindate=lastlogindate;
        this.createtime=createtime;
        this.loginstatus=loginstatus;
        this.note=note;
    }
    
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }
    
    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }
    
    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    public Integer getPhotoid() {
        return photoid;
    }

    public void setPhotoid(Integer photoid) {
        this.photoid = photoid;
    }
    
    public Date getLastlogindate() {
        return lastlogindate;
    }

    public void setLastlogindate(Date lastlogindate) {
        this.lastlogindate = lastlogindate;
    }
    
    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }
    
    public String getLoginstatus() {
        return loginstatus;
    }

    public void setLoginstatus(String loginstatus) {
        this.loginstatus = loginstatus;
    }
    
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}