package com.example.ssmproject;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = {"com.example.ssmproject.mapper"})
public class SsmprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsmprojectApplication.class, args);
    }

}
