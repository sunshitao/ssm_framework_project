package com.example.ssmproject.mapper;

import com.example.ssmproject.model.TUser;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Delete;
import java.util.List;

/**
 * (TUser)表数据库访问层
 *
 * @author makejava
 * @since 2019-12-26 23:13:27
 */
public interface TUserMapper {

    /**
     * 通过ID查询单条数据
     * @param userid 主键
     * @return 实例对象
     */
     @Select("select * from dbo.T_User where userid = #{userid}")
    TUser selectById(@Param("userid") String userid);

    /**
     * 查询指定行数据
     *
     * @param page 查询页数
     * @param pageSize 查询条数
     * @return 对象列表
     */
     @Select("select top #{pageSize} * from dbo.T_User where userid not in (select top (#{page}*#{pageSize}-1) userid from dbo.T_User)")
    List<TUser> selectAllByPage(@Param("page") int page, @Param("pageSize") int pageSize);

    /**
     * 通过实体作为筛选条件查询
     *
     * @return 对象列表
     */
     @Select("select * from dbo.T_User")
    List<TUser> selectAll();

    /**
     * 新增数据
     *
     * @param tUser 实例对象
     * @return 影响行数
     */
     @Insert("insert into dbo.T_User(username, realname, roleid, pwd, phone, status, photoid, lastlogindate, createtime, loginstatus, note) values (#{username}, #{realname}, #{roleid}, #{pwd}, #{phone}, #{status}, #{photoid}, #{lastlogindate}, #{createtime}, #{loginstatus}, #{note})")
    int insert(TUser tUser);

    /**
     * 通过主键删除数据
     *
     * @param userid 主键
     * @return 影响行数
     */
     @Delete("delete from dbo.T_User where userid = #{userid}")
    int deleteById(@Param("userid") String userid);

}