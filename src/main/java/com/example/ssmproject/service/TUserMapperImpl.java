package com.example.ssmproject.service;

import com.example.ssmproject.model.TUser;
import com.example.ssmproject.mapper.TUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (TUser)表服务实现类
 *
 * @author makejava
 * @since 2019-12-26 23:13:26
 */
@Primary
@Service
public class TUserMapperImpl implements TUserMapper {

    @Autowired
    private TUserMapper tUserDao;

    /**
     * 通过ID查询单条数据
     *
     * @param userid 主键
     * @return 实例对象
     */
    @Override
    public TUser selectById(String userid) {
        TUser tUser = tUserDao.selectById(userid);
        if(tUser != null && !tUser.equals("")){
            return tUser;
        }else{
            return null;
        }
    }

    /**
     * 查询多条数据
     *
     * @param page 查询页数
     * @param pageSize 查询条数
     * @return 对象列表
     */
    @Override
    public List<TUser> selectAllByPage(int page, int pageSize) {
        List<TUser> tUserlist = tUserDao.selectAllByPage(page, pageSize);
        if(tUserlist != null && tUserlist.size()>0){
            return tUserlist;
        }else{
            return null;
        }
    }
    
    /**
     * 查询所有数据
     *
     * @return 实例对象
     */
    @Override
    public List<TUser> selectAll() {
        List<TUser> tUserlist = tUserDao.selectAll();
        if(tUserlist != null && tUserlist.size()>0){
            return tUserlist;
        }else{
            return null;
        }
    }

    /**
     * 新增数据
     *
     * @param tUser 实例对象
     * @return 实例对象
     */
    @Override
    public int insert(TUser tUser) {
        return tUserDao.insert(tUser);
    }

    /**
     * 通过主键删除数据
     *
     * @param userid 主键
     * @return 是否成功
     */
    @Override
    public int deleteById(String userid) {
        return tUserDao.deleteById(userid);
    }
}